var App = (function (axios, Mustache) {
	var genres = [];
	var movies = [];

	var $genres = document.getElementById('genres');
	var $movies = document.getElementById('movies');

	var genresTpl = document.getElementById('genres-template').innerHTML;
	var moviesTemplate = document.getElementById('movies-template').innerHTML;
	var descTemplate = document.getElementById('description-template').innerHTML;

	return {
		getCurrentGenre: function () {
		},
		getMoviesByGenre: function () {
		},
		start: function () {
			var self = this;

			this.attachEvents();
			this.fetch()
			.then(function (response) {
				var data = response.data;
				genres = data.genres;
				movies = data.movies;

				self.createLinks();
			});
		},
		fetch: function (cb) {
			return axios.get('db.json');
		},
		createLinks: function () {
			$genres.innerHTML = Mustache.render(genresTpl, {
				genres: genres
			});
		},
		updateMovies: function () {
			//Filter movies by genre
			let mojHash = window.location.hash.substr(2);
			let filteredMovies = movies.reduce(function(acc, movie) {
				if (movie.genres.includes(mojHash) === true) {
					acc.push(movie);
				}
				return acc;
			}, []);
			
			//Print out filtered movies
			$movies.innerHTML = Mustache.render(moviesTemplate, {
				movies: filteredMovies
			});

			//New popup window with description that fires on click
			var openedWindow = null;

			$movies.addEventListener('click', function(e) {
				var idMovie = e.target.id;
				var clickedMovie = filteredMovies.filter(function(item) {
					return item.id == idMovie;
					});

				var left = (screen.width/2)-(650/2);
				var top = (screen.height/2)-(450/2);
				openedWindow = window.open('', 'new_window', 'location=yes, height=450,width=650, top='+top+', left='+left+', scrollbars=yes,status=yes')
				openedWindow.document.title = 'Movie Description';
				openedWindow.document.head.innerHTML = '<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">';
				openedWindow.document.body.innerHTML =
					Mustache.render(descTemplate, {
						movies: clickedMovie
					});
					openedWindow.focus();

			});

		},
		attachEvents: function () {
			window.addEventListener('hashchange', this.updateMovies.bind(this));
		}
	}
})(axios, Mustache);